<!doctype html>
<html>
	<head>
		<meta character="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=Edge,chrome=1">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

		<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/favicon.png" />
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>"/>
		<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'template_url' ); ?>/vendor/colorbox-master/example3/colorbox.css" />

		<?php wp_head(); ?>

		<?php wp_title(); ?>

		<!-- main -->
		<script src="<?php echo get_template_directory_uri(); ?>/vendor/from.js"></script>
		<script src="<?php bloginfo( 'template_url' ); ?>/vendor/modernizr.custom.js"></script>
	</head>
	<body>

		<!-- carosello preload -->
		<?php 
			$images = get_posts(array(
				'posts_per_page' => -1,
				'orderby' => 'menu_order',
				'order'=>'ASC',
				'post_type' => 'attachment',
				'post_status' => 'any',
				'post_parent' => $post->ID
			));
		
			foreach($images as $image){
				list($src, $width, $height) = wp_get_attachment_image_src($image->ID, 'full');
				
		?>	
			<div 
			    class="carouser-placeholder"
				data-src="<?php echo $src ?>" 
				data-width="<?php echo $width ?>" 
				data-height="<?php echo $height?>" 
				data-alt="<?php echo get_post_meta($image->ID, '_wp_attachment_image_alt', true) ?>"
			></div>
		
		<?php
			}
		?>


		<!-- carosello -->
		<a href="#" id="portfolio" class="anchor-portfolio"></a>
		<div class="scroll-wrap">
			<section class="main-scroll" id="main-scroll">

				<div class="inner-wrap hide">
					<div class="carousel-card last-card db">
						<img src="<?php echo get_template_directory_uri(); ?>/img/more.jpg" class='scroll-image db'>
						<a class="continue-artstation" href="https://www.artstation.com/niji" target="_blank">
							<span>
								Explore more on<br/>ArtStation.com
							</span>
						</a>
					</div>
				</div>
			</section>
		</div>


		<!-- navigazione -->
		<div class="main-head">
			<div id="decoration-top"></div>
			<div class="box">
				<div class="nav-title cf">
					<img src="<?php echo get_template_directory_uri(); ?>/img/niji-logo.png" class="logo-image">
					<span class="nav-head">
						<span>Francesca Resta</span> digital painter
					</span>
				</div>
				<div class="contact-email">
					contact: <a href="mailto:&#102;&#114;&#97;&#110;&#99;&#101;&#115;&#99;&#97;&#64;&#110;&#105;&#106;&#105;&#97;&#114;&#116;&#46;&#105;&#116;">&#102;&#114;&#97;&#110;&#99;&#101;&#115;&#99;&#97;&#64;&#110;&#105;&#106;&#105;&#97;&#114;&#116;&#46;&#105;&#116;</a>
				</div>
				<?php wp_nav_menu(array(
					'theme_location' => 'primary_menu',
					'container' => 'nav',
					'container_class' => 'main-nav'
				)); ?>
			</div>
			<!--<div id="logobadge"></div>-->
		</div>

		<!-- about me -->

		<article class="text main-box">
			<a href="#" id="bio" class="anchor"></a>
			<h1 name="bio">About me</h1>
			<img id="me" src="<?php echo get_template_directory_uri(); ?>/img/me-bw.jpg" />
			<?php
				$page_data = get_page_by_title('Bio');
				echo apply_filters('the_content', $page_data->post_content);
			?>
			<div style="clear: both"></div>
			<?php
				$page_data2 = get_page_by_path('clients'); ?>
				<h2><?php echo get_the_title($page_data2->ID); ?> </h2>
			<?php
				echo apply_filters('the_content', $page_data2->post_content);
			?>

			<?php
				$hl_category = get_category_by_slug('highlight');
				$highlight = get_posts(array(
					'posts_per_page' => 1,
					'orderby' => 'date',
					'category' => $hl_category->term_id
					));
					?>
			<article class="text right top" style="display:none;">
				<?php echo apply_filters('the_content', $highlight[0]->post_content); ?>
				<?php echo get_the_post_thumbnail( $highlight[0]->ID, 'thumbnail' ); ?>
			</article>

			<article class="text right top">
				<a href="https://www.painterartist.com/en/product/painter/#composition-guidance" href="corel painter featured artist" target="_blank"><img id="painter" src="<?php echo get_template_directory_uri(); ?>/img/painterFA.jpg"/></a>
			</article>

			<article id="contact-me" class="text right center">
				<p>You can contact me <!--through<br/><a href="http://www.astound.us/" target="_blank">my agent</a> or --><a href="mailto:&#102;&#114;&#97;&#110;&#99;&#101;&#115;&#99;&#97;&#64;&#110;&#105;&#106;&#105;&#97;&#114;&#116;&#46;&#105;&#116;">directly by email</a>.</p>
				<!--<div class="advocate">
					<a href="http://www.advocate-art.com/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/advo-logo.png"/></a>
					<a href="mailto:mail@advocate-art.com">mail@advocate-art.com</a><br/>
					56 The Street, Ashtead<br/>
					Surrey, KT21 1AZ, UK<br/>
					+44 (0) 20 8879 1166<br/>
					<span>illustrate your point</span>
				</div>
				<a href="http://www.astound.us/" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/astound-us-logo.png"/</a>-->
				<a href="mailto:&#102;&#114;&#97;&#110;&#99;&#101;&#115;&#99;&#97;&#64;&#110;&#105;&#106;&#105;&#97;&#114;&#116;&#46;&#105;&#116;"><img src="<?php echo get_template_directory_uri(); ?>/img/mailme.png"/></a>
			</article>

			<article class="text right">
				<h1 name="social">You can find me also on...</h1>
				<!--<p>Here are some communities and social network where you can also find me and follow my updates, sketches and more:</p>-->
				<!--
					<a href="http://niji707.deviantart.com" href="my page on DeviantArt" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/da-logo.png" class="sociallogo"/></a>
					<a href="http://nijiart.tumblr.com" href="my page on Tumblr" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/tumblr-logo.png" class="sociallogo"/></a>
					<a href="http://twitter.com/francesca707" href="my page on Twitter" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/twitter-logo.png" class="sociallogo"/></a>
				-->
				<a href="https://www.artstation.com/niji" title="my page on ArtStation" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/artstation_logo.png" class="sociallogo"/></a>
				<a href="https://www.facebook.com/nijiarts" title="my page on Facebook" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/facebook-logo.png" class="sociallogo"/></a>
				<a href="https://www.instagram.com/niji707/" title="my page on Instagram" target="_blank"><img src="<?php echo get_template_directory_uri(); ?>/img/instagram-logo.png" class="sociallogo"/></a>
			</article>
		</article>



		<!-- libri -->
		<?php
			$books_category = get_category_by_slug( 'book' );
			$books = get_posts(array(
				'posts_per_page' => -1,
				'orderby' => 'date',
				'category' => $books_category->term_id
			));
		?>


		<section class="booklist">
			<a href="#" id="books" class="anchor"></a>
			<h1>A selection of books with my paintings on the cover</h1>
			<?php foreach ($books as $book) {
				$thumb_id = get_post_thumbnail_id($book->ID);
				$fullcover = get_post($thumb_id);
				$fullcover_src = wp_get_attachment_image_src($thumb_id, 'full')
				?>
				<article class="book">
					<?php echo get_the_post_thumbnail( $book->ID, 'thumbnail' ); ?>
					<a class="details bookgallery" href="<?php echo $fullcover_src[0]; ?>" title="<?php echo $fullcover->post_excerpt ?>">
						<h3><?php echo $book->post_title?></h3>
						<p><?php echo get_post_meta($book->ID, 'autore', true); ?></p>
						<p><?php echo get_post_meta($book->ID, 'editore', true); ?></p>
					</a>
					<?php
						$args = array(
							'post_type' => 'attachment',
							'numberposts' => -1,
							'post_status' =>'any',
							'post_parent' => $book->ID,
							'orderby' => 'menu_order',
							'order'=>'DESC',
							'post__not_in'=>array(get_post_thumbnail_id($book->ID))
							);
						$images = get_posts($args);
					?>
					<div class="others">
						<?php foreach ($images as $image) {
							$thumb = wp_get_attachment_image_src($image->ID, 'thumbnail');
							$full=wp_get_attachment_image_src($image->ID, 'full');
							?>

							<a class="bookgallery" href="<?php echo $full[0]?>" title="<?php echo $image->post_excerpt ?>">
								<img src="<?php echo $thumb[0]?>"/>
							</a>
						<?php }?>
					</div>
				</article>
			<?php }?>
		</section>

		<?php wp_footer(); ?>

		<!-- main -->
		<script
			data-main="<?php echo get_template_directory_uri(); ?>/js/application"
			src="<?php echo get_template_directory_uri(); ?>/vendor/require.js">
		</script>

		<!-- Facebook Pixel Code -->
		<script>
		!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
		n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
		n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
		t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
		document,'script','https://connect.facebook.net/en_US/fbevents.js');

		fbq('init', '995691387191521');
		fbq('track', "PageView");</script>
		<noscript><img height="1" width="1" style="display:none"
		src="https://www.facebook.com/tr?id=995691387191521&ev=PageView&noscript=1"
		/></noscript>
		<!-- End Facebook Pixel Code -->

	</body>
</html>
