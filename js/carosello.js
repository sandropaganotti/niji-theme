define(['jquery','underscore','iscroll'], function(){

	var Carosello = function(ele, rightMargin){

		_.bindAll(this);

		this.raw = ele;
		this.ele = $('#' + ele);
		this.div = $('> div', this.ele);
		this.img = $('.carousel-card',this.ele);
		this.rightMargin = rightMargin || 0;

		this.ele.on('mousewheel',function(evt){
			if(Math.abs(evt.originalEvent.wheelDeltaX) > 0)
				evt.preventDefault();		
		});

		this.start();
		$(window).resize();
	};

	Carosello.prototype.start = function(){
		this.init();

		this.scroll = new iScroll(this.raw,{
			vScroll: false,
			hScrollbar: false,
			vScrollbar: false,
			bounce: false
		});

		$(window).on('resize',this.refresh);
	};

	Carosello.prototype.refresh = function(){
		var that = this;

		setTimeout(function () {
			that.init();
			that.scroll.refresh();
		}, 0);
	};

	Carosello.prototype.init = function() {
		var b1 =0, b2 = 0;
		this.img.each(function(i,e){
			b1 += $(e).outerWidth(true);
		});
		this.div.width(Math.max(b1,b2) + this.rightMargin);
	};

	return Carosello;

});