/* -- transparent images hack -- */
var canvas = document.createElement('canvas');
var innerWrap = document.querySelector('.inner-wrap'); 
var images = Array.from(document.querySelectorAll('.carouser-placeholder')).reverse();
for (var i = 0; i < images.length; i++) {
	canvas.width = parseInt(images[i].getAttribute('data-width'), 10);
	canvas.height = parseInt(images[i].getAttribute('data-height'), 10);
	var src = canvas.toDataURL('image/png');
	innerWrap.insertAdjacentHTML(
		'afterbegin',
		'<img id="card-' + i +'" class="carousel-card scroll-image db" src="' + src + '">',
	);
}

/* -- config -- */
require.config({
	paths: {
		'jquery': '../vendor/jquery.min',
		'iscroll': '../vendor/iscroll-IE7-8/src/iscroll',
		'domReady': '../vendor/domReady',
		'underscore': '../vendor/underscore-min',
		'colorbox': '../vendor/colorbox-master/jquery.colorbox-min'
	},
	shim: {
		'colorbox': {
			deps: ['jquery']
		}
	}
});

/* -- main -- */
require(['jquery','carosello','domReady!','colorbox'], function($, Carosello){

	/* -- replace placeholder -- */
	for (var i = images.length - 1; i >= 0; i--) {
		(function(x) {
			var image = new Image();
			image.src = images[x].getAttribute('data-src');
			image.addEventListener('load', function() {
				document.getElementById('card-' + x).src = image.src;	
			});	
		})(i);
	}

	/* -- remove spinner --*/
	$('.inner-wrap').removeClass('hide');

	/* -- carosello -- */
	var mainscroll = new Carosello('main-scroll', 50);

	/* -- colorbox -- */
	$('.book').each(function(i, book){
		$('.bookgallery', book).colorbox({
			rel: "book"+i,
			maxWidth: "90%",
			maxHeight: "90%",
			fixed: true
		});
	});

	/* -- disappear only iphone -- */
	if((navigator.userAgent.match(/iPhone/i)) || (navigator.userAgent.match(/iPod/i))) {
		
		// -- toggle about me and portfolio -- 
		var bio = $('a[href="#bio"]').parent(),
				portfolio = $('a[href="#portfolio"]').parent();

		bio.on('click', function(){
			$(this).hide();
			portfolio.show();
		});

		portfolio.on('click', function(){
			$(this).hide();
			bio.show();
		});

		// -- hide url bar --
		window.scrollTo(0, 1);
	}
	
});
